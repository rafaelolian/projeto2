var app = angular.module('app', [ 'ngTouch', 'ui.grid', 'ui.grid.selection' ]);

app.controller('endereco', ['$scope','$http', function($scope, $http) {
		
	$scope.consultarEndereco = function() {
		var cep = {"numero" : $scope.endereco.cep};

		$http.post('api/cep', cep)
		.success(function(data, status, headers, config) {
			$scope.endereco.rua = data.rua;
			$scope.endereco.bairro = data.bairro;
			$scope.endereco.cidade = data.cidade;
			$scope.endereco.estado = data.estado;
			$scope.mensagem = "";
		})
		.error(function(data, status, headers, config) {
			$scope.mensagem = data.message;
			$scope.endereco = {"cep" : $scope.conta.cep};
		});
	}

	$scope.enviar = function() {
		if( $scope.endereco.id ){
			$scope.atualizarEndereco();
		} else {
			$scope.enviarNovoEndereco();
		}
	}
	
	$scope.enviarNovoEndereco = function() {
		$http.post('api/endereco', $scope.endereco)
		.success(function(data, status, headers, config) {
			$scope.mensagem = "Endereço cadastrado com sucesso";
			$scope.endereco = {};
			$scope.atualizarGrid();
		})
		.error(function(data, status, headers, config) {
			if( status == 400 ){
				$scope.mensagem = "Preencha todos os campos obrigatórios (*)";
			} else {
				$scope.mensagem = "Erro ao enviar endereço: "+ data.message;
			}
		});		
	}

	$scope.atualizarEndereco = function() {
		$http.put('api/endereco/'+ $scope.endereco.id, $scope.endereco)
		.success(function(data, status, headers, config) {
			$scope.mensagem = "Endereço atualizado com sucesso";
			$scope.endereco = {};
			$scope.atualizarGrid();
		})
		.error(function(data, status, headers, config) {
			if( status == 400 ){
				$scope.mensagem = "Preencha todos os campos obrigatórios (*)";
			} else {
				$scope.mensagem = "Erro ao enviar endereço: "+ data.message;
			}
		});		
	}

	$scope.remover = function(){
		var selectedRows = $scope.enderecoGrid.selection.getSelectedRows();
		if( selectedRows && selectedRows.length == 1 ){
			$http.delete('api/endereco/'+ selectedRows[0].id ).success(function(data) {
				$scope.atualizarGrid();
				$scope.mensagem = "Endereço removido com sucesso";
			})
			.error(function(data, status, headers, config) {
				$scope.mensagem = "Erro ao remover endereço: "+ data.message;
			});
		} else {
			$scope.mensagem = "Nenhum endereço selecionado para remoção";
		}
	}

	$scope.editar = function(){
		$scope.mensagem = "";
		var selectedRows = $scope.enderecoGrid.selection.getSelectedRows();
		if( selectedRows && selectedRows.length == 1 ){
			$http.get('api/endereco/'+ selectedRows[0].id ).success(function(data) {
				$scope.endereco = data;
			});			
		} else {
			$scope.mensagem = "Nenhum endereço selecionado para edição";
		}
	}

	$scope.novo = function(){
		$scope.endereco = {};
		$scope.mensagem = "";
	}

	$scope.gridOptions = {
		enableRowSelection: true, 
		enableRowHeaderSelection: false,
		multiSelect: false,
		columnDefs : [ 
		    {field : 'rua'}, 
		    {field : 'bairro'}, 
		    {field : 'cidade'}, 
		    {field : 'estado'}, 
		    {field : 'cep'}, 
		    {field : 'numero'}, 
		    {field : 'complemento'} 
		]
	};
	
	$scope.atualizarGrid = function() {
		$http.get('api/endereco').success(function(data) {
			$scope.gridOptions.data = data;
		});
	}
	
	$scope.gridOptions.onRegisterApi = function(enderecoGrid){
		$scope.enderecoGrid = enderecoGrid;
	}

	$scope.atualizarGrid();
	
}]);