package br.com.projeto2.repository;

import java.util.Collection;

import javax.persistence.EntityManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.projeto2.domain.Endereco;

@Repository
public class EnderecoRepository {

	private final EntityManager entityManager;
	
	@Autowired
	public EnderecoRepository( final EntityManager entityManager ){
		this.entityManager = entityManager;
	}
	
	public Endereco cadastrar(Endereco endereco) {
		return entityManager.merge(endereco);
	}

	public Endereco buscarPorId(Long id) {
		return entityManager.find(Endereco.class, id);
	}

	public void removerPorId(Long id) {
		entityManager.remove( buscarPorId(id) );
	}

	@SuppressWarnings("unchecked")
	public Collection<Endereco> buscarTodos() {
		return entityManager.createQuery("from "+ Endereco.class.getSimpleName() ).getResultList() ;
	}

	public Endereco atualizar(Endereco endereco) {
		return entityManager.merge(endereco);
	}
}