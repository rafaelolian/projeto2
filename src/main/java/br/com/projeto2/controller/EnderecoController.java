package br.com.projeto2.controller;

import java.util.Collection;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.projeto2.domain.Endereco;
import br.com.projeto2.service.EnderecoService;

@RestController
@RequestMapping("/api/endereco")
public class EnderecoController {

	private final EnderecoService enderecoService;
	
	@Autowired
	public EnderecoController( final EnderecoService enderecoService ){
		this.enderecoService = enderecoService;
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Endereco cadastrarEndereco( @Valid @RequestBody final Endereco endereco ){
		return enderecoService.cadastrarEndereco(endereco);
	}

	@RequestMapping(method=RequestMethod.PUT, value="/{id}")
	public Endereco atualizarEndereco(@PathVariable final Long id, @Valid @RequestBody final Endereco endereco ){
		endereco.setId(id);
		return enderecoService.atualizarEndereco(endereco);
	}

	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	public Endereco buscarEnderecoPorId( @PathVariable final Long id ){
		return enderecoService.buscarEnderecoPorId(id);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	public void removerEnderecoPorId( @PathVariable final Long id ){
		enderecoService.removerEnderecoPorId(id);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public Collection<Endereco> buscarTodosEnderecos(){
		return enderecoService.buscarTodosEnderecos();
	}
}