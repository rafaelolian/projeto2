package br.com.projeto2.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.projeto2.domain.Cep;
import br.com.projeto2.domain.Endereco;
import br.com.projeto2.exceptions.CepInvalido;
import br.com.projeto2.repository.EnderecoRepository;

@Service
@Transactional
public class EnderecoService {

	private final CepService cepService;
	
	private final EnderecoRepository enderecoRepository;
	
	@Autowired
	public EnderecoService( final CepService cepService, final EnderecoRepository enderecoRepository ){
		this.cepService = cepService;
		this.enderecoRepository = enderecoRepository;
	}
	
	public Optional<Endereco> buscarEnderecoPorCep(final Cep cep) throws CepInvalido {
		if( cep.isValido() ){
			return buscarEnderecoPorCepValido(cep);
		} else {
			throw new CepInvalido();
		}
	}
	
	protected Optional<Endereco> buscarEnderecoPorCepValido( final Cep cep ){
		Optional<Endereco> endereco = cepService.buscarEndereco(cep.getNumero());
		
		if( ! endereco.isPresent() ){
			if( cep.incluirZeroADireita() ){
				endereco = buscarEnderecoPorCepValido(cep);
			}
		}
		
		return endereco;
	}

	public Endereco cadastrarEndereco(final Endereco endereco) {
		return enderecoRepository.cadastrar(endereco);
	}

	public Endereco buscarEnderecoPorId(final Long id) {
		return enderecoRepository.buscarPorId(id);
	}

	public void removerEnderecoPorId(final Long id) {
		enderecoRepository.removerPorId(id);
	}

	public Collection<Endereco> buscarTodosEnderecos() {
		return enderecoRepository.buscarTodos();
	}

	public Endereco atualizarEndereco(Endereco endereco) {
		return enderecoRepository.atualizar(endereco);
	}
}