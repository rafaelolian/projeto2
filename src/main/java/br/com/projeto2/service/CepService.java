package br.com.projeto2.service;

import java.util.Optional;

import br.com.projeto2.domain.Endereco;

public interface CepService {

	Optional<Endereco> buscarEndereco(String cep);

}