package br.com.projeto2.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

import java.nio.charset.Charset;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import br.com.projeto2.Application;
import br.com.projeto2.domain.Endereco;
import br.com.projeto2.service.CepServiceMock;

import static org.hamcrest.Matchers.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EnderecoControllerTest {
	
	private MockMvc mockMvc;
	
	private static Endereco endereco; 
	
	private MediaType contentType = new MediaType(MediaType.APPLICATION_JSON.getType(),
            MediaType.APPLICATION_JSON.getSubtype(),
            Charset.forName("utf8"));	
	
	@Autowired
    private WebApplicationContext webApplicationContext;
	
	@Before
    public void setup() throws Exception {
        this.mockMvc = webAppContextSetup(webApplicationContext).build();
	}

	@Test
	public void step1_deveCadastrarEndereco() throws Exception {
		String result = mockMvc.perform(post("/api/endereco")
				.content( getJson(getEnderecoValido()) )
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", notNullValue()))
				.andReturn().getResponse().getContentAsString();
		
		endereco = getObject(result, Endereco.class);
	}

	@Test
	public void step2_deveBuscarEnderecoPorId() throws Exception {
		mockMvc.perform(get("/api/endereco/"+ endereco.getId())
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(endereco.getId().intValue())));
	}

	@Test
	public void step3_deveAtualizarEndereco() throws Exception {
		final String novaRua = "Nova Rua Teste";
		endereco.setRua(novaRua);
		
		mockMvc.perform(put("/api/endereco/"+ endereco.getId())
				.content( getJson(endereco) )
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.id", is(endereco.getId().intValue())))
				.andExpect(jsonPath("$.rua", is(novaRua)));
	}

	@Test
	public void step4_deveBuscarTodosEnderecos() throws Exception {
		mockMvc.perform(get("/api/endereco")
				.contentType(contentType))
				.andExpect(status().isOk())
				.andExpect(jsonPath("$", hasSize(1)));
	}
	
	@Test
	public void step5_deveExcluirEndereco() throws Exception {
		mockMvc.perform(delete("/api/endereco/"+ endereco.getId())
				.contentType(contentType))
				.andExpect(status().isOk());
	}

	@Test
	public void step6_deveRejeitarEnderecoInvalido() throws Exception {
		Endereco enderecoInvalido = getEnderecoValido();
		enderecoInvalido.setRua(null);
		mockMvc.perform(post("/api/endereco")
				.content( getJson(enderecoInvalido) )
				.contentType(contentType))
				.andExpect(status().is4xxClientError());
		
	}

	private String getJson( Object object ) throws JsonProcessingException{
		return new ObjectMapper().writeValueAsString(object); 
	}
	
	private <T> T getObject(String json, Class<T> type) throws Exception{
		return new ObjectMapper().readValue(json, type);
	}
	
	private Endereco getEnderecoValido(){
		final Endereco endereco = CepServiceMock.ENDERECO_VALIDO;
		endereco.setNumero(943);
		return endereco;
	}
	
}
