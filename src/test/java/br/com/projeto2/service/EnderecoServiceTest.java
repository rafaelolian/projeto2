package br.com.projeto2.service;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

import br.com.projeto2.domain.Cep;
import br.com.projeto2.domain.Endereco;
import br.com.projeto2.exceptions.CepInvalido;
import br.com.projeto2.repository.EnderecoRepository;
import br.com.projeto2.service.CepServiceMock;
import br.com.projeto2.service.EnderecoService;

public class EnderecoServiceTest {
	
	private static final Long ID_ENDERECO_VALIDO = 1L;

	private EnderecoService enderecoService; 

	@Mock
	private EnderecoRepository enderecoRepository;
	
	@Before
	public void init(){
		MockitoAnnotations.initMocks(this);
		
		doReturn( getEnderecoValidoComId() ).when(enderecoRepository).cadastrar( any(Endereco.class) );
		doReturn( getEnderecoValidoComId() ).when(enderecoRepository).atualizar( any(Endereco.class) );
		doReturn( Arrays.asList(getEnderecoValidoComId()) ).when(enderecoRepository).buscarTodos();
		doReturn( getEnderecoValidoComId() ).when(enderecoRepository).buscarPorId(ID_ENDERECO_VALIDO);
		
		enderecoService = new EnderecoService(new CepServiceMock(),enderecoRepository);
	}
	
	@Test
	public void deveCadastrarEndereco(){
		final Endereco endereco = enderecoService.cadastrarEndereco(getEnderecoValidoSemId());
		verify(enderecoRepository, times(1)).cadastrar(endereco);
		Assert.assertNotNull(endereco.getId());
	}
	
	@Test
	public void deveAtualizarEndereco(){
		final Endereco enderecoValidoComId = getEnderecoValidoComId();
		final Endereco endereco = enderecoService.atualizarEndereco(enderecoValidoComId);
		verify(enderecoRepository, times(1)).atualizar(endereco);
		Assert.assertEquals(enderecoValidoComId.getId(), endereco.getId());		
	}
	
	@Test
	public void deveRemoverEndereco(){
		enderecoService.removerEnderecoPorId(ID_ENDERECO_VALIDO);
		verify(enderecoRepository, times(1)).removerPorId(ID_ENDERECO_VALIDO);
	}
	
	@Test
	public void deveBuscarTodosEnderecos(){
		final Collection<Endereco> enderecos = enderecoService.buscarTodosEnderecos();
		Assert.assertEquals(1, enderecos.size());
		verify(enderecoRepository, times(1)).buscarTodos();
	}

	@Test
	public void deveBuscarEnderecoPorId(){
		final Endereco endereco = enderecoService.buscarEnderecoPorId(ID_ENDERECO_VALIDO);
		Assert.assertEquals(ID_ENDERECO_VALIDO, endereco.getId());
		verify(enderecoRepository, times(1)).buscarPorId(ID_ENDERECO_VALIDO);
	}

	
	
	
	@Test(expected=CepInvalido.class)
	public void deveRetornarExcecaoParaCepInvalido(){
		enderecoService.buscarEnderecoPorCep( new Cep("1122X2333") );
	}

	@Test
	public void deveRetornarEnderecoParaCepValidoExistente(){
		final Optional<Endereco> endereco = enderecoService.buscarEnderecoPorCep( new Cep(CepServiceMock.CEP_EXISTENTE) );
		Assert.assertTrue(endereco.isPresent());
	}
	
	@Test
	public void naoDeveRetornarEnderecoParaCepValidoInexistente(){
		final Optional<Endereco> endereco = enderecoService.buscarEnderecoPorCep( new Cep("00000-000") );
		Assert.assertFalse(endereco.isPresent());
	}

	@Test
	public void deveRetornarEnderecoParaCepValidoCompletadoComZeroADireita(){
		final Optional<Endereco> endereco = enderecoService.buscarEnderecoPorCep( new Cep("01504999") );
		Assert.assertTrue(endereco.isPresent());
		Assert.assertEquals(CepServiceMock.CEP_ZERADO, endereco.get().getCep());
	}
	
	private Endereco getEnderecoValidoSemId(){
		final Endereco endereco = CepServiceMock.ENDERECO_VALIDO;
		endereco.setNumero(943);
		return endereco;
	}

	private Endereco getEnderecoValidoComId(){
		final Endereco endereco = getEnderecoValidoSemId();
		endereco.setId(ID_ENDERECO_VALIDO);
		return endereco;
	}
}