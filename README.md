# Projeto2

Desenvolver um CRUD (Create, Read, Update e Delete) para endereço do usuário.

## Estratégia
Desenvolver um projeto leve e altamente testável utilizando Java 8 e ferramentas fortemente consolidadas no mercado de desenvolvimento de *software* corporativo.

## Arquitetura

Visando atender a estratégia estabelecida, o projeto foi desenvolvido utilizando as tecnologias:

* Java 8
* Maven
* Spring Boot
* JUnit
* AngularJS
* Angular UI-Grid
* REST Apis
* Jetty
* MySQL
* H2

O projeto utiliza dois arquivos de configuração application.properties:

* projeto2/src/main/resources/application.properties
* projeto2/src/test/resources/application.properties

O primeiro contem as configurações do ambiente de desenvolvimento, apontando para o banco MySQL, o segundo contem as configuração do ambiente de teste automatizado, utilizando o banco de dados H2 em memória.


## Padrões

Foram adotados alguns padrões e boas práticas no desenvolvimento do Projeto2, visando facilitar o entendimento e melhorar o design do código fonte:

* MVC - Model View Controller
* Injeção de dependência 
* Inversão de controle
* Objetos imutáveis
* REST APIs
* TDD - Test Driven Development

## API
Os serviços para a API de endereço estão disponíveis em:

```
#!java

/projeto2/api/endereco
```

Segue abaixo exemplos de utilização da API:

* Criar endereço
```
#!java

POST: http://localhost:8080/projeto2/api/endereco
{
    "rua":"Rua Vergueiro",
    "bairro":"Liberdade",
    "cidade":"São Paulo",
    "estado":"SP",
    "cep":"01504001",
    "numero":100,
    "complemento":"segundo andar"
}
```

Resposta:


```
#!java

{
    "id": 1,
    "rua": "Rua Vergueiro",
    "bairro": "Liberdade",
    "cidade": "São Paulo",
    "estado": "SP",
    "cep": "01504001",
    "numero": 100,
    "complemento": "segundo andar"
}
```
* Atualizar endereço

```
#!java

PUT: http://localhost:8080/projeto2/api/endereco/1
{
    "rua":"Rua Vergueiro Teste",
    "bairro":"Liberdade",
    "cidade":"São Paulo",
    "estado":"SP",
    "cep":"01504001",
    "numero":101,
    "complemento":"segundo andar"
}

```

Resposta:


```
#!java

{
    "id": 1,
    "rua": "Rua Vergueiro Teste",
    "bairro": "Liberdade",
    "cidade": "São Paulo",
    "estado": "SP",
    "cep": "01504001",
    "numero": 101,
    "complemento": "segundo andar"
}
```

* Buscar endereço por ID


```
#!java

GET: http://localhost:8080/projeto2/api/endereco/1

```

Resposta:


```
#!java

{
    "id": 1,
    "rua": "Rua Vergueiro Teste",
    "bairro": "Liberdade",
    "cidade": "São Paulo",
    "estado": "SP",
    "cep": "01504001",
    "numero": 101,
    "complemento": "segundo andar"
}
```

* Buscar todos endereços

```
#!java

GET: http://localhost:8080/projeto2/api/endereco

```
Resposta:

```
#!java
[
  {
    "id": 1,
    "rua": "Rua Vergueiro Teste",
    "bairro": "Liberdade",
    "cidade": "São Paulo",
    "estado": "SP",
    "cep": "01504001",
    "numero": 101,
    "complemento": "segundo andar"
  }
]
```

* Excluir endereço por ID

```
#!java

DELETE: http://localhost:8080/projeto2/api/endereco/1

```

Resposta:
```
#!java

HTTP CODE: 200

```

* Criar endereço sem preencher campo obrigatório
```
#!java

POST: http://localhost:8080/projeto2/api/endereco
{
    "rua":null,
    "bairro":"Liberdade",
    "cidade":"São Paulo",
    "estado":"SP",
    "cep":"01504001",
    "numero":100,
    "complemento":"segundo andar"
}
```

Resposta:
```
#!java
HTTP CODE: 400

```

## Interface

Foi desenvolvida uma interface para disponibilizar as funcionalidades via WEB, a interface foi desenvolvida utilizando HTML, AngularJS e Angular UI-Grid, ela esta disponível no endereço:

```
#!java

/projeto2/endereco.html
```